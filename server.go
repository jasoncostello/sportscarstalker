package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Car struct {
	MongoId     string `json:"mongoId"`
	Model       string `json:"model"`
	Make        string `json:"make"`
	Year        string `json:"year"`
	DateFound   string `json:"dateFound"`
	PictureLink string `json:"pictureLink"`
}

var cars []Car

func GetCarEndpoint(w http.ResponseWriter, req *http.Request) {
	params := mux.Vars(req)
	for _, item := range cars {
		if item.MongoId == params["MongoId"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Car{})
}

func GetCarsEndpoint(w http.ResponseWriter, req *http.Request) {
	json.NewEncoder(w).Encode(cars)
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/car", GetCarsEndpoint).Methods("GET")
	router.HandleFunc("/car/{id}", GetCarEndpoint).Methods("GET")
	log.Fatal(http.ListenAndServe(":12345", router))
}
