package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)
import _ "github.com/go-sql-driver/mysql"

var cars = []car{}

type car struct {
	id        string
	make      string
	model     string
	modelYear string
	color     string
	longitude string
	latitude  string
	dateFound string
}

//db password -  ??--three-blinded-donkeys-today--??
func getCarsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	j, _ := json.Marshal(cars)
	w.Write(j)
}

func postCarHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var c car
	b, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(b, &c)

	cars = append(cars, c)

	j, _ := json.Marshal(cars)
	w.Write(j)

}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/car", getCarsHandler).Methods("GET")
	r.HandleFunc("/car", getCarsHandler).Methods("POST")
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
